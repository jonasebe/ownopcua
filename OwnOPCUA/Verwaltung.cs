﻿/********************************************/
/*      Clara Werner                        */
/*      Mechatronisches Projekt SS18        */
/*      Mai/18 - Juli/18                    */
/********************************************/
using System;
using System.Collections.Generic;
using Opc.Ua;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;

namespace OwnOPCUA
{
    public class Verwaltung
    {
        public event EventHandler reloadAdressSpace;
        
        public dynamic config = new JObject();
        public dynamic param = new JObject();
        public dynamic connections = new JObject();
        public dynamic signal = new JObject();

        public string pathOfAllJsonFiles;

        private void fireMyevent()
        {
            reloadAdressSpace?.Invoke(this, EventArgs.Empty);
        }

        public Verwaltung(string path)
        {            
            //objecte zuweisen
            config.Name = "default";
            config.KonfType = "String";
            config.DefaultValue = 0;
            config.DataRange = "";
            config.Value = "";

            param.Name = "default";
            param.ValueType = "Single";
            param.DefaultValue = 0;
            param.Value = 0;
            param.EngUnit = "%";

            connections.Name = "default";
            connections.Source = "EX";
            connections.SignalType = "AnalogInput";

            signal.Name = "default";
            signal.Visible = false;

            pathOfAllJsonFiles = path;
        }

        //newJsonObject: object, das befüllt wird, nameOfType: Wert, der bei "Type" stehen soll,  nrOfConfig/nrOfParam/nrOfConnections/nrOfSignals: Anzahl der Object 
        public void CreateAllObjOfJsonFile(ref dynamic newJsonObject, string nameOfType, string nrOfConfig, string nrOfParam, string nrOfConnections, string nrOfSignals)
        {
            dynamic functionsblock = new JObject();
            //Standard Eigenschaften der Json Files
            functionsblock.Type = nameOfType;
            functionsblock.Number = 0;

            //gewünschte Anzahl an Objekten hinzufügen
            dynamic ConfigData = new JObject();
            for (int i = 0; i < Convert.ToInt32(nrOfConfig); i++)
            {
                ConfigData["Config_" + i.ToString()] = config;
            }

            dynamic ParamData = new JObject();
            for (int i = 0; i < Convert.ToInt32(nrOfParam); i++)
            {
                ParamData["Param_" + i.ToString()] = param;
            }

            dynamic ConnectionData = new JObject();
            for (int i = 0; i < Convert.ToInt32(nrOfConnections); i++)
            {
                ConnectionData["Channel_" + i.ToString()] = connections;
            }

            dynamic SignalData = new JObject();
            for (int i = 0; i < Convert.ToInt32(nrOfSignals); i++)
            {
                SignalData["Signal_" + i.ToString()] = signal;
            }

            //alle Objekte dem Hauptobjekt hinzufügen
            functionsblock.ConfigData = ConfigData;
            functionsblock.Parameter = ParamData;
            functionsblock.Connections = ConnectionData;
            functionsblock.Signals = SignalData;
            newJsonObject.functionsblock = functionsblock;
        }

        //Ein neues json file wird erzeugt - Event, das Methode zugeordnet wird
        public ServiceResult CreateNewJsonFile(ISystemContext context, MethodState method, IList<object> inputArguments, IList<object> outputArguments)
        {
            // all arguments must be provided.
            if (inputArguments.Count < 6)
            {
                return StatusCodes.BadArgumentsMissing;
            }

            // check the data type of the input arguments.
            uint? configNr = inputArguments[2] as uint?;
            uint? paramNr = inputArguments[3] as uint?;
            uint? channelNr = inputArguments[4] as uint?;
            uint? signalNr = inputArguments[5] as uint?;

            if (configNr == null || paramNr == null || channelNr == null || signalNr == null)
            {
                return StatusCodes.BadTypeMismatch;
            }

            //neues file im richtigen Pfad erzeugen
            using (var myFile = File.Create(pathOfAllJsonFiles + "\\" + inputArguments[1].ToString() + ".json")) { }

            //neues Objekt, das nachher als json file gespeichert wird
            dynamic neuesObject = new JObject();

            //neuesObject wird bearbeitet nach Wunsch des Users. 
            CreateAllObjOfJsonFile(ref neuesObject, inputArguments[0].ToString(), inputArguments[2].ToString(), inputArguments[3].ToString(), inputArguments[4].ToString(), inputArguments[5].ToString());

            //neuesObject wird in string umgewandelt, welcher wiederrum ins neu erzeugten File geschrieben wird
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(neuesObject, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(pathOfAllJsonFiles + "\\" + inputArguments[1].ToString() + ".json", output);

            //AdressSpace wird neu ausgelesen, damit im Clienten wieder aktuelle Json files angezeigt werden
            //myNodeManager.invokeCreateAdressSpace();
            fireMyevent();
            return ServiceResult.Good;
        }

        //Ein json file wird gelöscht- Event, das Methode zugeordnet wird
        public ServiceResult DeleteJFile(ISystemContext context, MethodState method, IList<object> inputArguments, IList<object> outputArguments)
        {
            //entsprechendes json file wird gelöscht. Pfad wird aus nodeId.Identifier herausgelesen. Denn beim Erzeugen der Node wird Name des 
            //zugehörigen json files dort mit abgespeichert (siehe CreateXXX() -> NodeID)
            File.Delete(pathOfAllJsonFiles + "\\" + method.Parent.NodeId.Identifier.ToString().Split('-').First().ToString());

            //AdressSpace wird neu ausgelesen, damit im Clienten wieder aktuelle Json files angezeigt werden
            //myNodeManager.invokeCreateAdressSpace();
            fireMyevent();
            return ServiceResult.Good;
        }

        //Stelle im objekt finden, an der der Wert geändert wurde
        //valuePath: Pfad zur Stelle, an dem Wert geändert wurde, jsonObj: object, in dem gesucht werden muss und indem Wert überschrieben wird
        //valueToFind: Wert, der geändert worden ist, value: geänderter Wert (gespeichert als object)
        public bool FindValue(string[] valuePath, ref dynamic jsonObj, string valueToFind, int nr, ref object value)
        {
            if (valuePath[nr] == valueToFind)
            {
                //wenn Stelle gefunden wurde, Wert ersetzen
                //Switch case Anwendung, damit richtig gecastet werden kann.
                switch (value.GetType().Name)
                {
                    case "String":
                        jsonObj[valuePath[nr]] = value.ToString();
                        break;
                    case "Int32":
                        jsonObj[valuePath[nr]] = (int)value;
                        break;
                    case "Boolean":
                        jsonObj[valuePath[nr]] = (bool)value;
                        break;
                    default:
                        break;
                }
                return true;
            }

            //altes Objekt speichern und in diesem weiter suchen
            dynamic newObject = jsonObj[valuePath[nr]];
            //nr um eins erhören, um an nächste Stelle von valuePath zu gelangen
            nr++;

            //prüfen, ob Stelle des geänderten Wertes schon gefunden wurde, wenn nein weitersuchen, ansonsten Suche beenden
            if (FindValue(valuePath, ref newObject, valueToFind, nr, ref value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ServiceResult ChangedValueOfProp(ISystemContext context, NodeState node, NumericRange indexRange, QualifiedName dataEncoding, ref object value, ref StatusCode statusCode, ref DateTime timestamp)
        {
            //Einlesen der entsprechenden json datei und in object speichern
            dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(File.ReadAllText(pathOfAllJsonFiles + "\\" + node.NodeId.Identifier.ToString().Split('-').First()));

            //Pfad zur Stelle des geänderten Wertes
            string[] pathSplit = node.NodeId.Identifier.ToString().Split('-').Last().Split('.');
            //Property, die geändert wurde
            string valueToFind = node.NodeId.Identifier.ToString().Split('.').Last().ToString();

            //richtige Stelle im object, das die Ínfos des json files enthält, finden
            FindValue(pathSplit, ref jsonObj, valueToFind, 0, ref value);

            //object in als json- string speichern und wieder in alte Datei schreiben
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(pathOfAllJsonFiles + "\\" + node.NodeId.Identifier.ToString().Split('-').First(), output);

            return ServiceResult.Good;
        }
    }
}
