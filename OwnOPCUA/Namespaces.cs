﻿/********************************************/
/*      Clara Werner                        */
/*      Mechatronisches Projekt SS18        */
/*      Mai/18 - Juli/18                    */
/********************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OwnOPCUA
{
    class Namespaces
    {
        public const string ReferenceApplications = "http://opcfoundation.org/Quickstarts/ReferenceApplications";
    }
}
