﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

using Opc.Ua;
using Opc.Ua.Server;
using Opc.Ua.Configuration;

namespace OwnOPCUA
{
    public partial class Form1 : Form
    {
        protected ApplicationInstance m_app;
        protected MyServer m_server;

        public Form1()
        {
            InitializeComponent();
            m_app = new ApplicationInstance();
            m_app.ApplicationType = ApplicationType.Server;
            m_app.ConfigSectionName = "MyServer";
            textBox2.Text = "opc.tcp://localhost:4842/MyOPCServer";


            Task<ApplicationConfiguration> task = m_app.LoadApplicationConfiguration(false);

            // check the application certificate.
            Task<bool> task2 = m_app.CheckApplicationInstanceCertificate(false, 0);
            task2.Wait();

            Task task3 = m_app.Start(new MyServer());
            task3.Wait();

            m_server = (MyServer)m_app.Server;
            Debug.Print(m_app.ApplicationConfiguration.ApplicationUri);
            
        }
    }
}
