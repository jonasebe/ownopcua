﻿/********************************************/
/*      Clara Werner                        */
/*      Mechatronisches Projekt SS18        */
/*      Mai/18 - Juli/18                    */
/********************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Opc.Ua;
using Opc.Ua.Server;
using Newtonsoft.Json.Linq;

namespace OwnOPCUA
{
    public class NodeManager : CustomNodeManager2
    {
        public Verwaltung myVerwaltung ;
        public FolderState m_root;
        public IDictionary<NodeId, IList<IReference>> externalReferences;
        public List<NodeId> m_references;
        public JObject o1 = new JObject();
        public string pathOfAllJsonFiles = @"C:\Users\User\Documents\git\repositories\OWNOpcServer\JSONFiles";
        
        public void InvokeCreateAdressSpaceFirstTime()
        {
            CreateAddressSpace(externalReferences);
        }

        //AdressSpace wird nochmal geladen. Dadurch können im Clienten aktuelle Json Files und Werte angezeigt werden
        public void invokeCreateAdressSpace()
        {
            foreach (NodeId item in m_references)
            {
                DeleteNode(SystemContext, item);
            }

            MethodeToCreateNewJFile(m_root);

            lock (Lock)
            {
                //alle Json Files aus bestimmten Ornder abspeichern
                DirectoryInfo di = new DirectoryInfo(pathOfAllJsonFiles);

                //alle Json Files durchlaufen und OPCUA Objekte erzeugen
                foreach (var jsonFile in di.GetFiles("*.json"))
                {
                    var dir = jsonFile.Directory;
                    String path = dir.ToString() + "\\" + jsonFile.Name;
                    //erstellen der ordners Anzeiger, in welchem die objecte sich wiederfinden
                    FolderState AnzeigerOrdner = CreateFolder(m_root, GetObject(path), GetObject(path), path);
                    MethodeToDeleteJfile(AnzeigerOrdner);
                    //CreateMethod(AnzeigerOrdner);
                    o1 = JObject.Parse(File.ReadAllText(path));

                    //JSON- file durchlaufen und neue Objekte bzw. wenn keine Kinder mehr vorhanden Properties erstellen
                    foreach (var item in o1[o1.First.Path.ToString()])
                    {
                        foreach (var item1 in item)
                        {
                            if (item1.HasValues)
                            {
                                GetAllNodes(item1, CreateObject(item1, AnzeigerOrdner, path), path);
                            }
                            else
                            {
                                CreateProperty(AnzeigerOrdner, item1, path);
                            }
                        }
                    }
                }

                AddPredefinedNode(SystemContext, m_root);
            }
        }

        private ServerConfiguration m_configuration;

        void reload_event(object sender, EventArgs param)
        {
            invokeCreateAdressSpace();
        }

        //Konstruktor
        //Konfigurationen werden festgelegt. Objekte, aus denen json files bestehen, werden initialisiert
        public NodeManager(IServerInternal server, ApplicationConfiguration configuration) : base(server, configuration, Namespaces.ReferenceApplications)
        {
            m_references = new List<NodeId>();
            SystemContext.NodeIdFactory = this;
            myVerwaltung = new Verwaltung(pathOfAllJsonFiles);
            myVerwaltung.reloadAdressSpace += new EventHandler(reload_event);
            // get the configuration for the node manager.
            m_configuration = configuration.ServerConfiguration;
            // use suitable defaults if no configuration exists.
            if (m_configuration == null)
            {
                m_configuration = new ServerConfiguration();
            }
        }

        //Wert von "Type" bekommen -> wird Name des OPC UA Ordners 
        //path: Pfad im Explorer, an dem das JsonFile liegt
        public string GetObject(string path)
        {
            JObject o1 = JObject.Parse(File.ReadAllText(path));
            return o1[o1.First.Path.ToString()]["Type"].ToString();
        }

        //Casten des neu geschriebenen Wertes einer Property
        //item: Item, das keine Kinder mehr hat und für das eine Property angelegt werden soll,  newProp: neu erstellte Property für item
        private void ValueTypeOfProperty(ref JToken item, ref PropertyState newProp)
        {
            switch (item.Type)
            {
                case JTokenType.String:
                    newProp.DataType = DataTypeIds.String;
                    newProp.Value = item.ToString();
                    break;

                case JTokenType.Integer:
                    newProp.DataType = DataTypeIds.Integer;
                    newProp.Value = (int)item;
                    break;

                case JTokenType.Boolean:
                    newProp.DataType = DataTypeIds.Boolean;
                    newProp.Value = (bool)item;
                    break;

                default:
                    newProp.DataType = DataTypeIds.String;
                    newProp.Value = item.ToString();
                    break;
            }
        }

        //OPC UA Ordner erstellen
        //parent: NodeState, dem der Ordner untergeordnet werden soll, path: Pfad, an dem Ordner erstellt werden soll, name: Name des Ordners, path2: Pfad zum entsprechenden Json File
        private FolderState CreateFolder(NodeState parent, string path, string name, string path2)
        {
            FolderState folder = new FolderState(parent);

            folder.SymbolicName = name;
            folder.ReferenceTypeId = ReferenceTypes.Organizes;
            folder.TypeDefinitionId = ObjectTypeIds.FolderType;
            //NodeId besteht aus json file Name + Pfad. Somit unique
            folder.NodeId = new NodeId(path2.Split('\\').Last().ToString() + "-" + path, NamespaceIndex);
            folder.BrowseName = new QualifiedName(path, NamespaceIndex);
            folder.DisplayName = new LocalizedText("en", name);
            folder.WriteMask = AttributeWriteMask.None;
            folder.UserWriteMask = AttributeWriteMask.None;
            folder.EventNotifier = EventNotifiers.SubscribeToEvents;

            if (parent != null)
            {
                parent.AddChild(folder);
            }

            m_references.Add(folder.NodeId);
            return folder;
        }

        //OPC UA Objekt erstellen
        //item: item, von dem ein Objekt erstellt werden soll, parent: NodeState, dem das Objekt untergeordnet werden soll, path: Pfad des Json Files
        public BaseObjectState CreateObject(JToken item, NodeState parent, string path)
        {
            BaseObjectState objects = new BaseObjectState(null);
            //NodeId besteht aus Name des json files + Pfad zum Objekt
            objects.NodeId = new NodeId(path.Split('\\').Last().ToString() + "-" + item.Path, NamespaceIndex);
            objects.BrowseName = new QualifiedName(item.Path.ToString().Split('.')[item.Path.ToString().Split('.').Length - 1], NamespaceIndex);
            objects.DisplayName = objects.BrowseName.Name;
            objects.TypeDefinitionId = ObjectTypeIds.BaseObjectType;

            objects.EventNotifier = EventNotifiers.SubscribeToEvents;
            m_references.Add(objects.NodeId);
            parent.AddChild(objects);
            return objects;
        }

        //OPC UA Property erstellen
        //parent: ObjectState, dem Property zugeordnet wird, path: Pfad des Json Files
        public void CreateProperty(BaseObjectState parent, JToken item, string path)
        {
            PropertyState prop = new PropertyState(parent);
            //NodeId besteht aus Name des entsprechenden json files + Pfad zur Stelle der Property
            prop.NodeId = new NodeId(path.Split('\\').Last().ToString() + "-" + item.Path, NamespaceIndex);
            prop.BrowseName = new QualifiedName(item.Path.ToString().Split('.')[item.Path.ToString().Split('.').Length - 1], NamespaceIndex);
            prop.DisplayName = prop.BrowseName.Name;
            prop.TypeDefinitionId = VariableTypeIds.PropertyType;
            prop.ReferenceTypeId = ReferenceTypeIds.HasProperty;

            //Typ der Property wird durch eine switch-case- Anweisung festgelegt
            ValueTypeOfProperty(ref item, ref prop);

            prop.ReferenceTypeId = ReferenceTypeIds.Organizes;
            prop.ValueRank = ValueRanks.Scalar;

            //Alle Properties haben Lese- und Schreibrechte
            prop.AccessLevel = AccessLevels.CurrentReadOrWrite;
            prop.UserAccessLevel = AccessLevels.CurrentReadOrWrite;
            prop.WriteMask = AttributeWriteMask.DisplayName | AttributeWriteMask.Description;
            prop.UserWriteMask = AttributeWriteMask.DisplayName | AttributeWriteMask.Description;

            prop.ClearChangeMasks(SystemContext, false);
            parent.AddChild(prop);

            //Event wird beim Abändern des Wertes im Clienten getriggert
            prop.OnWriteValue = myVerwaltung.ChangedValueOfProp;
            // prop.OnWriteValue = ChangedValueOfProp;

            m_references.Add(prop.NodeId);
        }

        //Mit dieser Methode kann ein json file gelöscht werden
        //parent: NodeState, der Methode hinzugefügt werden soll
        public void MethodeToDeleteJfile(NodeState parent)
        {
            MethodState start = new MethodState(parent);

            start.NodeId = new NodeId(2, NamespaceIndex);
            start.BrowseName = new QualifiedName("DeleteFile", NamespaceIndex);
            start.DisplayName = start.BrowseName.Name;
            start.ReferenceTypeId = ReferenceTypeIds.HasComponent;
            start.UserExecutable = true;
            start.Executable = true;


            parent.AddChild(start);
            // set up method handlers. 
            start.OnCallMethod = new GenericMethodCalledEventHandler(myVerwaltung.DeleteJFile);

            //start.OnCallMethod = new GenericMethodCalledEventHandler(DeleteJFile);
        }

        //Mit Hilfe dieser Methode wird eine OPC UA Methode erstellt, mit der man vom Clienten aus ein neues json file erzeugen kann
        //Node State, der Methode hinzugefüt werden soll
        public void MethodeToCreateNewJFile(NodeState parent)
        {
            // a method to start the process.
            MethodState start = new MethodState(parent);

            start.NodeId = new NodeId(3, NamespaceIndex);
            start.BrowseName = new QualifiedName("ANewJsonFile", NamespaceIndex);
            start.DisplayName = start.BrowseName.Name;
            start.ReferenceTypeId = ReferenceTypeIds.HasComponent;
            start.UserExecutable = true;
            start.Executable = true;

            // add input arguments.
            start.InputArguments = new PropertyState<Argument[]>(start);
            start.InputArguments.NodeId = new NodeId("InputArgs" + parent.ToString(), NamespaceIndex);
            start.InputArguments.BrowseName = BrowseNames.InputArguments;
            start.InputArguments.DisplayName = start.InputArguments.BrowseName.Name;
            start.InputArguments.TypeDefinitionId = VariableTypeIds.PropertyType;
            start.InputArguments.ReferenceTypeId = ReferenceTypeIds.HasProperty;
            start.InputArguments.DataType = DataTypeIds.Argument;
            start.InputArguments.ValueRank = ValueRanks.OneDimension;

            //der User kann angeben wie seine neue json Datei heißen soll, wie der OPC UA Ordner im Clienten heißen soll
            //und wie viele Objekte er jeweils von Config, Parameter, Connections und Signals haben möchte
            Argument[] args = new Argument[6];
            args[0] = new Argument();
            args[0].Name = "Type: ";
            args[0].Description = "Type of functionblock (Name of folder in client)";
            args[0].DataType = DataTypeIds.String;
            args[0].ValueRank = ValueRanks.Scalar;

            args[1] = new Argument();
            args[1].Name = "Name of new File";
            args[1].Description = "Name of new Json file";
            args[1].DataType = DataTypeIds.String;
            args[1].ValueRank = ValueRanks.Scalar;

            args[2] = new Argument();
            args[2].Name = "Config Objects";
            args[2].Description = "Number of Config Objects";
            args[2].DataType = DataTypeIds.UInt32;
            args[2].ValueRank = ValueRanks.Scalar;

            args[3] = new Argument();
            args[3].Name = "Param Objects";
            args[3].Description = "Number of Param Objects";
            args[3].DataType = DataTypeIds.UInt32;
            args[3].ValueRank = ValueRanks.Scalar;

            args[4] = new Argument();
            args[4].Name = "Connections Objects";
            args[4].Description = "Number of Connections Objects";
            args[4].DataType = DataTypeIds.UInt32;
            args[4].ValueRank = ValueRanks.Scalar;

            args[5] = new Argument();
            args[5].Name = "Signals Objects";
            args[5].Description = "Number of Signals Objects";
            args[5].DataType = DataTypeIds.UInt32;
            args[5].ValueRank = ValueRanks.Scalar;
            start.InputArguments.Value = args;

            parent.AddChild(start);

            // Der Methode wird einen entsprechende Funktion zugewiesen 

            start.OnCallMethod = new GenericMethodCalledEventHandler(myVerwaltung.CreateNewJsonFile);
        }

        //JSON- file rekursiv auslesen und Objekte/Properties erstellen
        //next: item, das nun durchlaufen werden soll, parent: ObjektState, dem neues Objekt/Property zugeordnet wird, path: Pfad zum Json File
        public void GetAllNodes(JToken next, BaseObjectState parent, string path)
        {
            foreach (var item in next)
            {
                if (item.HasValues)
                {
                    foreach (var item2 in item)
                    {
                        if (item2.HasValues)
                        {
                            GetAllNodes(item2, CreateObject(item2, parent, path), path);
                        }
                        else
                        {
                            CreateProperty(parent, item2, path);
                        }
                    }
                }
                else
                {
                    CreateProperty(parent, item, path);
                }
            }
        }

        public override void CreateAddressSpace(IDictionary<NodeId, IList<IReference>> externalReferences)
        {
            lock (Lock)
            {
                IList<IReference> references = null;

                if (!externalReferences.TryGetValue(ObjectIds.ObjectsFolder, out references))
                {
                    externalReferences[ObjectIds.ObjectsFolder] = references = new List<IReference>();
                }

                //erstellen des "root"-ordners. Unter diesem Ordner werden alle Json files gelistet, die sich in dem Pfad "pathOfAllJsonFiles" befinden
                FolderState root = CreateFolder(null, "selectedJSON", "selectedJSON", "root\\root");
                root.AddReference(ReferenceTypes.Organizes, true, ObjectIds.ObjectsFolder);
                references.Add(new NodeStateReference(ReferenceTypes.Organizes, false, root.NodeId));
                root.EventNotifier = EventNotifiers.SubscribeToEvents;
                AddRootNotifier(root);

                //dem "Hauptordner" wird eine Methode hinzugefügt. Mit dieser Methode kann man über den Clienten neue Json files erstellen.
                MethodeToCreateNewJFile(root);

                //alle Json Files aus bestimmten Ornder abspeichern
                DirectoryInfo di = new DirectoryInfo(pathOfAllJsonFiles);

                //alle Json Files durchlaufen, die sich im "pathOfAllJsonFils" befinden und OPCUA Objekte und Properties erzeugen
                //foreach um durch Pfad zu laufen- di.GetFiles("*.json") gibt alle files mit der .json Endung zurück
                foreach (var jsonFile in di.GetFiles("*.json"))
                {
                    var dir = jsonFile.Directory;
                    //kompletter Pfad des aktuellen json files
                    String path = dir.ToString() + "\\" + jsonFile.Name;

                    //Ordner für das aktuelle json file wird erstellt. Der Name des Ordners entspricht dem Wert von "Type". 
                    //Name des Ordners bekmmt man über die Methode GetObject(string path) 
                    FolderState AnzeigerOrdner = CreateFolder(root, GetObject(path), GetObject(path), path);

                    //Jedem Ordner, der für ein json file steht, wird eine Methode hinzugefügt. Mit dieser Methode kann das file gelöscht werden.
                    MethodeToDeleteJfile(AnzeigerOrdner);

                    //auslesen des json files und erzeugen eines JObjectes mit dem Inhalt des json files
                    o1 = JObject.Parse(File.ReadAllText(path));

                    //komplettes JObject o1 wird rekursiv durchlaufen. Sobald ein item über keine Kinder mehr Verfügt, wird eine Property angelegt
                    //existieren Kinder, wird ein Objekt angelegt und es wird eine Ebene tiefer weiter gesucht
                    foreach (var item in o1[o1.First.Path.ToString()])
                    {
                        foreach (var item1 in item)
                        {
                            if (item1.HasValues)
                            {
                                //da Kinder existieren, wird ein Objekt mit der Methode CreateObject() erstellt und nach weiteren Nodes mit GetAllNodes() gesucht
                                GetAllNodes(item1, CreateObject(item1, AnzeigerOrdner, path), path);
                            }
                            else
                            {
                                //Methode CreateProerty() erstellt eine OPC UA Property
                                CreateProperty(AnzeigerOrdner, item1, path);
                            }
                        }
                    }
                }

                //Alle erzeugten Nodes müssen dem Hautpordner, als der root, hinzugefügt werden
                AddPredefinedNode(SystemContext, root);

                //m_root ist eine globale Variable und speicher den Hauptordner ab. Dies wird benötigt, da nach einem Aufruf von Methoden oder 
                //dem Ändern von Values das Verzeichnis der json files komplett neu gelesen wird und dadurch die Anzeige im Clienten aktualisiert wird
                //siehe InvokeCreateAdressSpace()
                m_root = root;
            }
        }

    }

}
